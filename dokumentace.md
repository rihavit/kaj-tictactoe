# Piškvorky (Gomoku)

## Cíle projektu
Vytvořit single-page webovou aplikaci, na které lze hrát hru piškvorky. Ovládání by mělo být uživatelsky přívětivé.
Systém musí kontrolovat pravidla. Aplikace bude postavená na JavaScript frameworku Vue.js.

## Postup
Dřive než jsem začal připravovat uživatelské rozhraní, vytvořil jsem model hry a hráče. Záměrem bylo aplikovat návrhový
vzor MVC. Poté, co byla vytvořena základní funkcionalita jsem začal pracovat na UI. Nejdůležitější byla práce s
canvasem. Ten se nachází ve vlastní Vue komponentě `Game.vue`, která má na starost vykreslování hrací plochy a kamenů.

Dlouhou dobu jsem pracoval pouze s jedním canvasem, ale ke konci vývoje jsem se rozhodl systém přepracovat a využít dva
canvasy, které se překrývají. Ten spodní vykresluje pouze herní plochu, tedy pozadí a čtverce herní plochy. Na svrchním
canvasu, který má průhledné pozadí, se vykreslují kameny hráčů (křížky a kolečka). To mi umožňuje při každé změně stavu
plochy smazat celý obsah svrchního canvasu a vykreslit všechny kameny.

Po prvotním zprovoznění vykreslování herní plochy jsem vytvořil Vue komponentu pro zobrazení modelu hráče, která
umožňuje změnu barvy a jména a zobrazuje hráčovo skóre.

Dlouho jsem odkládal přidání tlačítek "Undo", "Restart game" a "Reset score". Logika pro tyto funkce byla v modelu hry
vytvořena téměř od začátku, ale vždy jsem měl pocit, že jiné věci mají vyšší prioritu. Nakonec jsem je přidal, když už
mě nebavilo při každé výhře přenačítat celou stránku.

To nás přivádí k momentu, kdy jsem se rozhodl využít LocalStorage. Vadilo mi, že při přepnutí na obrazovku "About" se
ztratí postup hry i skóre. Implementace LocalStorage je vcelku jednoduchá. Veškerá data jsem převedl do řetězce ve
formátu JSON a uložil. Následné parsování tohoto řetězce při načítání stránky bylo také bezproblémové. Jediné, s čím
jsem nepočítal bylo, že je potřeba nastavit prototyp načteného objektu - pokud jsem uložil celou instanci objektu
typu Game, je potřeba načtenému objektu tento prototyp nastavit např. pomocí `Object.setPrototypeOf(storedData,
GameModel.prototype);`.

Dále bylo na řadě už jen finální ladění, oprava chyb - některé větší, některé drobné, a poté sepsat dokumentaci :).

### Popis modelů
Model hráče je jednoduchý v aplikace jako celku je vedlejší. Obsahuje informace jako jméno hráče, nebo jakou používá
barvu. 
 
Jádrem aplikace je model Game.  
Přehled atributů:
 - `size: Number` - velikost hrací plochy. Např. pro číslo `10` reprezentuje herní plochu 10x10.
 - `board: Array` - stav hrací plochy. Jendá se o pole čísel `0` nebo `1`, případně `null` nebo `undefined`. `0` a `1`
  reprezentují kameny hráčů, null a undefined reprezentují prázdná pole. Původně jsem uvažoval jen `undefined`, ale při
  parsování JSONu při načítání hry se prázdná místa v poli vyplnila jako `null`.
 - `score: Array` - pole o velikosti 2 reprezentuje skóre hráčů.
 - `history: Array` - historie tahů. Při každém tahu se na konec tohoto pole zapíše index v poli `board`, na kterém
  bylo v tom tahu taženo. Využívá se pro implementaci "Undo".
 - `turn: Number` - který hráč je na řadě (0 nebo 1).
 - `winner: Number` - který hráč vyhrál (0 nebo 1). Zapíše se na konci hry.
 - `winningLine: Object { from: Number, to: Number }` - obsahuje informaci o tom, kde se nachází vítězných pět kamenů
  v řadě. To je využito při vykreslení čáry, která přeškrtne tuto řadu. Atributy `from` a `to` reprezentují indexy v
  poli `board`.
  
## Popis funkcionality
### Hráči
Hráči si mohou měnit jméno a barvu. Tyto změny se ukládají a načtou se i po vypnutí prohlížeče.

Jméno lze změnit kliknutím na jméno hráče, jehož jméno si přejeme změnit. Po kliknutí se zobrazí vstupní pole,
automaticky se na něj prohlížeč zaměří a vybere text, který obsahuje. Po kliknutí mimo vstupní pole se nové jméno uloží
a místo vstupního pole se znovu zobrazí pouze text. Pokud uživatel smaže obsah pole a klikne mimo něj, změní se jméno
hráče na výchozí "Player 1" nebo "Player 2".

Barva se změní kliknutím na barevný obdélník pod jménem hráče. Zobrazí se dialogové okno podle daného prohlížeče. Změna
se uloží po zavření tohoto dialogového okna.

### Hra
#### Tah
Tah se provádí kliknutím levým tlačítkem miši na políčko, kam chce hráč umístit kámen. Který kráč je na tahu je vidět,
když se kurzor nachází nad herním polem - nad vybraným políčkem se zobrazí náhled stavu po umístění kamane. Umístění
kamene je doprovázeno zvukovým efektem. Po umístění kamene se automaticky přepne, který hráč je na tahu.

#### Vrácení tahu
Hráč má možnost vrátit tah, např. pokud se překlepl. Slouží k tomu tlačítko umístěné nad herní plochou s nápisem "Undo".
Vrátit lze libovolné množství tahů.

#### Konec hry
Pokud se některému z hráčů podaří spojit pět kamenů do řady, nastane konec hry. Hračí mají možnost začít novou hru
pomocí tlačítka "New game". Tlačítko "Restart game" nad herní plochou má stejný efekt. Pokud se hráči rozhodnou vrátit
poslední tah (pomocí tlačítek "Undo last move", nebo "Undo"), vrátí se skóre do stavu před koncem hry.  
Po začátku nové hry je první na tahu hráč, který prohrál předchozí hru.

#### Restart hry
Pokud se hráči rozhodnou v průběhu hry začít znovu, mohou tak učinit pomocí tlačítka "Restart game" nad herní plochou.  
Pozn.: V restartované hře začíná hráč, který byl na tahu před restartem hry! 
