import Vue from 'vue';

export default class Game {
  constructor(size) {
    this.size = size;
    this.board = [];
    this.score = [0, 0];
    this.history = [];
    this.turn = 0;
    this.winner = null;
    this.winningLine = null;
  }

  static getIndexFromCoordinates(coordinates, boardSize) {
    return coordinates.x + boardSize * coordinates.y;
  }

  static getCoordinatesFromIndex(index, boardSize) {
    const x = index % boardSize;
    const y = (index - x) / boardSize;
    return { x, y };
  }

  static getOffsetFromIndex(index, boardSize, tileSize, center) {
    if (center === undefined) center = false;
    const coordinates = Game.getCoordinatesFromIndex(index, boardSize);
    coordinates.x *= tileSize;
    coordinates.y *= tileSize;
    if (center) {
      coordinates.x += tileSize / 2;
      coordinates.y += tileSize / 2;
    }
    return coordinates;
  }

  toggleTurn() {
    this.turn = 1 - this.turn;
  }

  isOutOfBounds(coordinates) {
    return coordinates.x < 0 || coordinates.x > this.size - 1 ||
      coordinates.y < 0 || coordinates.y > this.size - 1;
  }

  checkLine(directionX, directionY) {
    let countPos = 0;
    let countNeg = 0;
    let from;
    let to;
    const lastMove = Game.getCoordinatesFromIndex(this.history.slice(-1).pop(), this.size);
    let curTile = Object.assign({}, lastMove);
    while (!this.isOutOfBounds(curTile) && this.board[Game.getIndexFromCoordinates(curTile, this.size)] === this.turn) {
      from = Object.assign({}, curTile);
      countPos += 1;
      curTile.x += directionX;
      curTile.y += directionY;
    }
    curTile = Object.assign({}, lastMove);
    while (!this.isOutOfBounds(curTile) && this.board[Game.getIndexFromCoordinates(curTile, this.size)] === this.turn) {
      to = Object.assign({}, curTile);
      countNeg += 1;
      curTile.x -= directionX;
      curTile.y -= directionY;
    }
    if (countPos + countNeg - 1 === 5) {
      const indexFrom = Game.getIndexFromCoordinates(from, this.size);
      const indexTo = Game.getIndexFromCoordinates(to, this.size);
      this.winningLine = { from: indexFrom, to: indexTo };
      return true;
    }
    return false;
  }

  checkWinCondition() {
    return this.checkLine(0, 1) ||
      this.checkLine(1, 0) ||
      this.checkLine(1, 1) ||
      this.checkLine(1, -1);
  }

  getSquareState(x, y) {
    return this.board[Game.getIndexFromCoordinates({ x, y }, this.size)];
  }

  recordMove(x, y) {
    if (this.winner !== null) throw new Error('Trying to play in a disabled game!');

    const index = Game.getIndexFromCoordinates({ x, y }, this.size);
    if (this.board[index] !== undefined && this.board[index] !== null) {
      return this.turn;
    }
    Vue.set(this.board, index, this.turn);
    this.history.push(index);
    if (this.checkWinCondition()) {
      this.winner = this.turn;
      this.score[this.turn] += 1;
    }
    this.toggleTurn();
    return this.turn;
  }

  undoMove() {
    Vue.set(this.board, this.history.pop(), undefined);
    this.toggleTurn();
    if (this.winner !== null) {
      this.score[this.winner] -= 1;
      this.winner = null;
      this.winningLine = null;
    }
    return this.turn;
  }

  resetGame() {
    this.winner = null;
    this.board = [];
    this.history = [];
    this.winningLine = null;
  }

  resetScore() {
    this.resetGame();
    this.turn = 0;
    this.score = [0, 0];
  }
}
