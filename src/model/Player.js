export default class Player {
  constructor(name, color, score) {
    this.name = name;
    this.color = color;
    this.score = score;
  }
}
